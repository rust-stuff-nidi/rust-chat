# Rust chat

- Set up the project locally running: `cargo run`
- Open the project in: http://localhost:8000/
- Open the app on different browsers or windows and chat.

Preview:

![image preview](./RUST_CHAT_PREVIEW.png "Screenshot of app preview")
