import React from 'react';
import { render, screen } from '@testing-library/react';
import Title from './Title';

describe('Title', () => {
  it('renders correctly', () => {
    render(<Title />);
    const headingNode = screen.getByRole('heading');
    expect(headingNode).toBeInTheDocument();
  });

  it('renders correct text', () => {
    render(<Title />);
    const headingNode = screen.getByText(/Rust chat/i);
    expect(headingNode).toBeInTheDocument();
  });
});
