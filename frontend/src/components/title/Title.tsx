import React, { FC } from 'react';
import { Container, Divider, Typography } from '@mui/material';
import { useTheme } from '@mui/material/styles';

const Title: FC = () => {
  const theme = useTheme();

  return (
    <Container>
      <Typography
        sx={{ my: 3, textAlign: 'center' }}
        variant="h3"
        component="h1"
      >
        Rust chat
      </Typography>
      <Divider
        sx={{
          borderColor: theme.palette.primary.main,
          borderWidth: '0.125px',
          width: '10rem',
          m: 'auto',
        }}
        variant="middle"
      />
    </Container>
  );
};
export default Title;
