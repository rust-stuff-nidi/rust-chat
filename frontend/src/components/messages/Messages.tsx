import React, { FC } from 'react';
import { Box, Typography } from '@mui/material';

interface Props {
  messages: Array<{ username: string; message: string }>;
}
const Messages: FC<Props> = ({ messages }) => {
  return (
    <Box
      data-testid="messages"
      sx={{ m: 'auto', width: '360px', height: '300px', overflow: 'auto' }}
    >
      {messages.map((m, ix) => (
        <Box key={`msg-${ix}`}>
          <Typography sx={{ fontWeight: 'bold' }}>{m.username}:</Typography>
          <Typography>{m.message}</Typography>
        </Box>
      ))}
    </Box>
  );
};

export default Messages;
