import React from 'react';
import { render, screen } from '@testing-library/react';
import Messages from './Messages';

describe('Messages', () => {
  const testProps = {
    messages: [
      {
        id: '1',
        username: 'Pipo',
        message: 'Hello world!',
      },
    ],
  };

  it('renders correctly', () => {
    render(<Messages {...testProps} />);
    const wrapperNode = screen.getByTestId('messages');
    expect(wrapperNode).toBeInTheDocument();
  });

  it('renders username and message', () => {
    render(<Messages {...testProps} />);
    const nodeUsername = screen.getByText(testProps.messages[0].username);
    const nodeMessage = screen.getByText(testProps.messages[0].message);

    expect(nodeUsername).toBeInTheDocument();
    expect(nodeMessage).toBeInTheDocument();
  });
});
