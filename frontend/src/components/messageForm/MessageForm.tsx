import React, { FC } from 'react';
import { Form, Formik } from 'formik';
import { Box, Button, Container, TextField } from '@mui/material';
import {
  initialValues,
  inputs,
  TypeFormValues,
  validateForm,
} from './MessageFormUtils';

interface Props {
  handleSubmit: (
    values: TypeFormValues,
    {
      setSubmitting,
      resetForm,
    }: {
      setSubmitting: (bool: boolean) => void;
      resetForm: () => void;
    }
  ) => void;
}

const MessageForm: FC<Props> = ({ handleSubmit }) => {
  return (
    <Container sx={{ my: 3, textAlign: 'center' }}>
      <Formik
        initialValues={initialValues}
        validate={validateForm}
        onSubmit={handleSubmit}
      >
        {({ isSubmitting, values, handleChange, touched, errors }) => (
          <Form data-testid="form-star">
            <Box
              sx={{
                display: 'flex',
                alignItems: 'top',
                justifyContent: 'center',
              }}
            >
              <>
                {inputs.map((input) => (
                  <Box key={input.id} sx={{ my: 1 }}>
                    <TextField
                      fullWidth
                      id={input.id}
                      name={input.id}
                      label={input.title}
                      type={input.type}
                      value={values[input.id]}
                      onChange={handleChange}
                      placeholder={input.placeholder}
                      error={touched[input.id] && Boolean(errors[input.id])}
                      helperText={touched[input.id] && errors[input.id]}
                      size="small"
                      sx={input.sx}
                      variant="outlined"
                    />
                  </Box>
                ))}
              </>
              <Button
                disabled={isSubmitting}
                type="submit"
                sx={{ m: 1, p: 1, height: 'fit-content' }}
                variant="outlined"
              >
                Send
              </Button>
            </Box>
          </Form>
        )}
      </Formik>
    </Container>
  );
};
export default MessageForm;
