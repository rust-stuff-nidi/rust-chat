import * as Yup from 'yup';

export type TypeFormValues = {
  username: string;
  message: string;
};

export type TypeInput = {
  id: keyof TypeFormValues;
  title: string;
  type: string;
  placeholder: string;
  sx: object;
};

export const inputs: TypeInput[] = [
  {
    id: 'username',
    title: 'Username',
    type: 'text',
    placeholder: 'Username',
    sx: { mr: 1, width: '7rem' },
  },
  {
    id: 'message',
    title: 'Message',
    type: 'text',
    placeholder: 'Message',
    sx: {},
  },
];

export const initialValues = {
  username: '',
  message: '',
};

export type TypeValidationError = {
  inner?: { path: string; errors: string[] }[];
};

export const getErrorsFromValidationError = (
  validationError: TypeValidationError
) =>
  validationError.inner?.reduce(
    (
      errors: { [key: string]: string },
      error: { path: string; errors: string[] }
    ) => {
      return {
        ...errors,
        [error.path]: error.errors[0],
      };
    },
    {}
  );

const validationSchema = Yup.object().shape({
  username: Yup.string().max(10, 'value is too long').required('required'),
  message: Yup.string().max(50, 'value is too long').required('required'),
});

export const validateForm = (values: TypeFormValues) => {
  try {
    validationSchema.validateSync(values, { abortEarly: false });
    return {};
  } catch (reason) {
    return getErrorsFromValidationError(reason as TypeValidationError);
  }
};
