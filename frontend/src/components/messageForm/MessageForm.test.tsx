import React from 'react';
import { render, screen } from '@testing-library/react';
import MessageForm from './MessageForm';

describe('Rooms', () => {
  const testProps = {
    handleSubmit: jest.fn(),
  };

  it('renders correctly', () => {
    render(<MessageForm {...testProps} />);
    const headingNode = screen.getByRole('heading');
    expect(headingNode).toBeInTheDocument();
  });
});
