import React, { FC, MouseEvent } from 'react';
import { Box, Button, Container } from '@mui/material';
import { initialRooms } from '../../utils/constants';

interface Props {
  selectedRoom: string;
  handleClickRoom: (ev: MouseEvent<HTMLButtonElement>, id: string) => void;
}
const Rooms: FC<Props> = ({ selectedRoom, handleClickRoom }) => {
  return (
    <Container sx={{ my: 3, textAlign: 'center' }}>
      <Box>
        {initialRooms.map((room) => (
          <Button
            key={room.id}
            onClick={(ev) => handleClickRoom(ev, room.id)}
            sx={{ m: 1 }}
            variant={room.id === selectedRoom ? 'contained' : 'outlined'}
          >
            {room.name}
          </Button>
        ))}
      </Box>
    </Container>
  );
};
export default Rooms;
