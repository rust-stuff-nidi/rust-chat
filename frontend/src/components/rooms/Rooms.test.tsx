import React from 'react';
import { render, screen } from '@testing-library/react';
import Rooms from './Rooms';

describe('Rooms', () => {
  const testProps = {
    selectedRoom: 'room-1',
    handleClickRoom: jest.fn(),
  };

  it('renders correctly', () => {
    render(<Rooms {...testProps} />);
    const headingNode = screen.getByRole('heading');
    expect(headingNode).toBeInTheDocument();
  });
});
