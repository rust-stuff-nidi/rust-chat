import React, { FC, MouseEvent, useEffect, useState } from 'react';
import { ThemeProvider } from '@mui/material/styles';
import CssBaseline from '@mui/material/CssBaseline';
import { theme } from '../../config/theme';
import { Box } from '@mui/material';
import Title from '../title/Title';
import Rooms from '../rooms/Rooms';
import Messages from '../messages/Messages';
import MessageForm from '../messageForm/MessageForm';
import { initialRooms } from '../../utils/constants';
import { TypeFormValues } from '../messageForm/MessageFormUtils';

const App: FC = () => {
  const [isConnected, setIsConnected] = useState(false);
  const [rooms, setRooms] = useState(initialRooms);
  const [selectedRoom, setSelectedRoom] = useState(rooms[0].id);
  const currentMessages =
    rooms.find((r) => r.id === selectedRoom)?.messages || [];

  const handleClickRoom = (ev: MouseEvent<HTMLButtonElement>, id: string) => {
    ev.preventDefault();
    setSelectedRoom(id);
  };

  const handleSubmitMessage = (
    values: TypeFormValues,
    {
      setSubmitting,
      resetForm,
    }: { setSubmitting: (bool: boolean) => void; resetForm: () => void }
  ) => {
    const { username, message } = values;
    if (isConnected) {
      fetch('/message', {
        method: 'POST',
        body: new URLSearchParams({ room: selectedRoom, username, message }),
      }).then((response) => {
        setSubmitting(false);
        resetForm();
      });
    }
  };

  const addMessage = (room: string, username: string, message: string) => {
    setRooms((prevState) =>
      prevState.map((r) =>
        r.id === room
          ? { ...r, messages: [...r.messages, { username, message }] }
          : r
      )
    );
  };

  useEffect(() => {
    const connect = (uri: string) => {
      const events = new EventSource(uri);

      events.addEventListener('message', (ev: any) => {
        try {
          const msg = JSON.parse(ev.data);

          if ('message' in msg && 'room' in msg && 'username' in msg) {
            addMessage(msg.room, msg.username, msg.message);
          }
        } catch (reason) {
          console.error(reason);
        }
      });

      events.addEventListener('open', () => {
        setIsConnected(true);
        console.log('Connected to event stream.');
      });

      events.addEventListener('error', () => {
        setIsConnected(false);
        events.close();

        console.log('Connection lost. Attempting to reconnect.');
        setTimeout(() => connect(uri), (() => 3000)());
      });
    };

    connect('/events');
  }, []);

  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <Box data-test-id="app">
        <Title />
        <Rooms selectedRoom={selectedRoom} handleClickRoom={handleClickRoom} />
        <Messages messages={currentMessages} />
        <MessageForm handleSubmit={handleSubmitMessage} />
      </Box>
    </ThemeProvider>
  );
};

export default App;
