import React from 'react';
import { render, screen } from '@testing-library/react';
import App from './App';

describe('App', () => {
  it('renders correctly', () => {
    render(<App />);
    const wrapperNode = screen.getByTestId('app');
    expect(wrapperNode).toBeInTheDocument();
  });
});
