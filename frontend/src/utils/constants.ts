type TypeRoom = {
  id: string;
  name: string;
  messages: { username: string; message: string }[];
};

export const initialRooms: TypeRoom[] = [
  {
    id: 'room-1',
    name: 'ROOM 1',
    messages: [],
  },
  {
    id: 'room-2',
    name: 'ROOM 2',
    messages: [],
  },
  {
    id: 'room-3',
    name: 'ROOM 3',
    messages: [],
  },
];
