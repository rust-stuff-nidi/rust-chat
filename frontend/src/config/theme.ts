import { createTheme } from '@mui/material/styles';
import { green } from '@mui/material/colors';

export const themeLight = createTheme({
  palette: {
    mode: 'light',
    background: {
      paper: '#FFF',
      default: '#FFF',
    },
    primary: {
      light: '#6450FF',
      main: '#0020BB',
      dark: '#001528',
      contrastText: '#FFF',
    },
  },
});

export const themeDark = createTheme({
  palette: {
    mode: 'dark',
    background: {
      paper: '#000',
      default: '#000',
    },
    primary: {
      light: green[300],
      main: green[500],
      dark: green[900],
      contrastText: '#FFF',
    },
  },
});

export const theme = themeDark;
